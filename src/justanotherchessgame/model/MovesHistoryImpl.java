package justanotherchessgame.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a log of all performed moves.
 */
public class MovesHistoryImpl implements MovesHistory {

    private final List<MoveInfo> moves;

    /**
     * Constructor of the Move History.
     */
    public MovesHistoryImpl() {
        moves = new ArrayList<MoveInfo>();
    }

    @Override
    public final boolean nextColor() {
        //After an even number of moves, white moves
        return moves.size() % 2 == 0;
    }

    @Override
    public final void addMove(final MoveInfo m) {
        moves.add(m);
    }

    @Override
    public final List<MoveInfo> getMoves() {
        return moves;
    }
}
